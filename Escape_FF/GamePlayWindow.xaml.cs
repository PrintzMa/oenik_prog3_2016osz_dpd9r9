﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Escape_FF
{
    /// <summary>
    /// Interaction logic for GamePlayWindow.xaml
    /// </summary>
    public partial class GamePlayWindow : Window
    {
        #region Fields

        /// <summary>
        /// üzleti logika
        /// </summary>
        BusinessLogic BL;

        /// <summary>
        /// játékos
        /// </summary>
        Player selectedPlayer;

        /// <summary>
        /// stopper
        /// </summary>
        Stopwatch sw;

        /// <summary>
        /// pálya szintje
        /// </summary>
        int levelNumber;

        #endregion

        #region Constructor

        /// <summary>
        /// a játék konstruktora
        /// </summary>
        /// <param name="selectedPlayer">játékos</param>
        public GamePlayWindow(Player selectedPlayer)
        {
            InitializeComponent();
            this.selectedPlayer = selectedPlayer;
        }

        #endregion

        #region EventHandlers

        /// <summary>
        /// Az ablak betöltése
        /// </summary>
        /// <param name="sender">s</param>
        /// <param name="e">e</param>
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            levelNumber = 1;
            BL = new BusinessLogic(levelNumber, selectedPlayer);
            this.DataContext = BL.MM;
            sw = new Stopwatch();
            sw.Start();
        }

        /// <summary>
        /// billentyű lenyomása
        /// </summary>
        /// <param name="sender">s</param>
        /// <param name="e">e</param>
        private void Window_KeyDown(object sender, KeyEventArgs e)
        {
            bool exit = false;
            switch (e.Key)
            {
                case Key.Up: exit = BL.Move(0, -1);
                    break;
                case Key.Down: exit = BL.Move(0, 1);
                    break;
                case Key.Left: exit = BL.Move(-1, 0);
                    break;
                case Key.Right: exit = BL.Move(1, 0);
                    break;
            }

            if (exit)
            {
                sw.Stop();
                int reward = levelNumber * 200 - (int)sw.Elapsed.Seconds;
                selectedPlayer.Gold += reward;
                MessageBox.Show("Elérted a kijáratot. A pálya teljesítéséért a jutalmad: " + reward.ToString() + " arany");
                if (levelNumber <= 5)
                {
                    if (levelNumber == 5)
                    {
                        MessageBox.Show("Gratulálok, elértél a játék végére, sikeresen túlélted!");
                        Application.Current.Shutdown();
                    }

                    selectedPlayer.Health = selectedPlayer.MaxHealth;
                    LevelUpWindow lvlUP = new LevelUpWindow(selectedPlayer);
                    lvlUP.ShowDialog();
                    levelNumber++;
                    BL = new BusinessLogic(levelNumber, selectedPlayer);
                    DataContext = BL.MM;
                    sw.Reset();
                    sw.Start();
                }
            }
        }

        #endregion
    }
}
