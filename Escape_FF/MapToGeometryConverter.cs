﻿namespace Escape_FF
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;
    using System.Windows.Data;
    using System.Windows.Media;

    /// <summary>
    /// térkép konvertálás
    /// </summary>
    class MapToGeometryConverter : IValueConverter
    {
        /// <summary>
        /// konverter
        /// </summary>
        /// <param name="value">v</param>
        /// <param name="targetType">t</param>
        /// <param name="parameter">p</param>
        /// <param name="culture">c</param>
        /// <returns>a térkép kép formájában</returns>
        public object Convert(object value, System.Type targetType, object parameter, CultureInfo culture)
        {
            bool[,] map = (bool[,])value;
            GeometryGroup geoMap = new GeometryGroup();
            for (int x = 0; x < map.GetLength(0); x++)
            {
                for (int y = 0; y < map.GetLength(1); y++)
                {
                    if (map[x, y])
                    {
                        RectangleGeometry tile = new RectangleGeometry(new Rect(x, y, 1, 1));
                        geoMap.Children.Add(tile);
                    }
                }
            }

            return geoMap;
        }

        /// <summary>
        /// visszakonvertáló
        /// </summary>
        /// <param name="value">v</param>
        /// <param name="targetType">t</param>
        /// <param name="parameter">p</param>
        /// <param name="culture">c</param>
        /// <returns>exception</returns>
        public object ConvertBack(object value, System.Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
