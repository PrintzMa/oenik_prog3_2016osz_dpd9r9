﻿namespace Escape_FF
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;
    using System.Windows.Media;
    using System.Windows.Media.Imaging;
    using GalaSoft.MvvmLight;

    /// <summary>
    /// mapmodel
    /// </summary>
    class MapModel : ObservableObject
    {
        #region Fields

        /// <summary>
        /// térkép
        /// </summary>
        bool[,] map;

        /// <summary>
        /// játékos
        /// </summary>
        Player selectedPlayer;

        /// <summary>
        /// kijárat pontja
        /// </summary>
        Point exit;

        /// <summary>
        /// ellenfelek a térképen
        /// </summary>
        List<Enemy> enemies;

        /// <summary>
        /// kincsek a tékrépen
        /// </summary>
        List<Treasure> treasures;

        #endregion

        #region Properties

        /// <summary>
        /// térkép tulajdonsága
        /// </summary>
        public bool[,] Map
        {
            get
            {
                return map;
            }

            set
            {
                Set(ref map, value);
            }
        }

        /// <summary>
        /// játékos tulajdonsága
        /// </summary>
        public Player SelectedPlayer
        {
            get
            {
                return selectedPlayer;
            }

            set
            {
                Set(ref selectedPlayer, value);
            }
        }

        /// <summary>
        /// kijárat tulajdonsága
        /// </summary>
        public Point Exit
        {
            get
            {
                return exit;
            }

            set
            {
                Set(ref exit, value);
            }
        }

        /// <summary>
        /// Ellenfelek tulajdonsága
        /// </summary>
        public List<Enemy> Enemies
        {
            get
            {
                return enemies;
            }

            set
            {
                Set(ref enemies, value);
            }
        }

        /// <summary>
        /// kincsek tulajdonsága
        /// </summary>
        public List<Treasure> Treasures
        {
            get
            {
                return treasures;
            }

            set
            {
                Set(ref treasures, value);
            }
        }

        /// <summary>
        /// fal képe
        /// </summary>
        public Brush WallBrush
        {
            get { return GetBrush("wall.jpg"); }
        }

        /// <summary>
        /// kijárat képe
        /// </summary>
        public Brush ExitBrush
        {
            get { return GetBrush("exit.bmp"); }
        }

        #endregion

        #region Constructor

        /// <summary>
        /// mapmodel konstruktora
        /// </summary>
        /// <param name="selectedPlayer">játékos</param>
        public MapModel(Player selectedPlayer)
        {
            this.selectedPlayer = selectedPlayer;
        }

        #endregion

        #region Methods

        /// <summary>
        /// kép rajzolása
        /// </summary>
        /// <param name="fileName">forrás</param>
        /// <returns>kép</returns>
        private Brush GetBrush(string fileName)
        {
            ImageBrush ib = new ImageBrush(new BitmapImage(new Uri(fileName, UriKind.Relative)));
            ib.TileMode = TileMode.Tile;
            ib.Viewport = new Rect(0, 0, 1, 1);
            ib.ViewportUnits = BrushMappingMode.Absolute;
            return ib;
        }

        #endregion
    }
}
