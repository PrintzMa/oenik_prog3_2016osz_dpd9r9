﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Escape_FF
{
    /// <summary>
    /// Interaction logic for CharacterChoosing.xaml
    /// </summary>
    public partial class CharacterChoosing : Window
    {
        #region Fields

        /// <summary>
        /// A játékos
        /// </summary>
        Player selectedPlayer;

        #endregion

        #region Constructor

        /// <summary>
        /// A karakterválasztás konstruktora
        /// </summary>
        public CharacterChoosing()
        {
            InitializeComponent();
        }

        #endregion

        #region EventHandlers

        /// <summary>
        /// Lovag választása
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e">e</param>
        private void btnKnight(object sender, RoutedEventArgs e)
        {
            selectedPlayer = new Player(Type.Knight, 100, 1, 10);
            GamePlayWindow gpw = new GamePlayWindow(selectedPlayer);
            this.Hide();
            gpw.ShowDialog();
        }

        /// <summary>
        /// Barbár választása
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e">e</param>
        private void btnBarbarian(object sender, RoutedEventArgs e)
        {
            selectedPlayer = new Player(Type.Barbarian, 100, 1, 10);
            GamePlayWindow gpw = new GamePlayWindow(selectedPlayer);
            this.Hide();
            gpw.ShowDialog();
        }

        /// <summary>
        /// Íjász választása
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e">e</param>
        private void btnArcher(object sender, RoutedEventArgs e)
        {
            selectedPlayer = new Player(Type.Archer, 100, 1, 10);
            GamePlayWindow gpw = new GamePlayWindow(selectedPlayer);
            this.Hide();
            gpw.ShowDialog();
        }

        #endregion
    }
}
