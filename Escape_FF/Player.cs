﻿namespace Escape_FF
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;
    using System.Windows.Media;
    using System.Windows.Media.Imaging;

    /// <summary>
    /// játékos típusa
    /// </summary>
    public enum Type { Knight, Barbarian, Archer }

    /// <summary>
    /// játékos
    /// </summary>
    public class Player : Character
    {
        #region Fields
        /// <summary>
        /// randon generálás
        /// </summary>
        static Random rnd = new Random();

        /// <summary>
        /// típus
        /// </summary>
        Type type;

        /// <summary>
        /// arany
        /// </summary>
        int gold;

        /// <summary>
        /// maximum életerő
        /// </summary>
        int maxHealth;

        /// <summary>
        /// életek száma
        /// </summary>
        int life;

        /// <summary>
        /// kezdő helyzet
        /// </summary>
        Point startLocation;

        /// <summary>
        /// kép
        /// </summary>
        Brush brush;

        #endregion

        #region Properties

        /// <summary>
        /// típus tulajdonsága
        /// </summary>
        public Type Type
        {
            get { return type; }
            set { Set(ref type, value); }
        }

        /// <summary>
        /// arany t.
        /// </summary>
        public int Gold
        {
            get
            {
                return gold;
            }

            set
            {
                Set(ref gold, value);
            }
        }

        /// <summary>
        /// életek t.
        /// </summary>
        public int Life
        {
            get
            {
                return life;
            }

            set
            {
                Set(ref life, value);
            }
        }

        /// <summary>
        /// kép t.
        /// </summary>
        public Brush Brush
        {
            get
            {
                return brush;
            }
        }

        /// <summary>
        /// maximum életerő t.
        /// </summary>
        public int MaxHealth
        {
            get
            {
                return maxHealth;
            }

            set
            {
                Set(ref maxHealth, value);
            }
        }

        /// <summary>
        /// kezdő helyzet t.
        /// </summary>
        public Point StartLocation
        {
            get
            {
                return startLocation;
            }

            set
            {
                Set(ref startLocation, value);
            }
        }

        #endregion

        #region Constructor

        /// <summary>
        /// játékos konstruktora
        /// </summary>
        /// <param name="type">típus</param>
        /// <param name="health">életerő</param>
        /// <param name="power">támadó erő</param>
        /// <param name="armor">páncélzat</param>
        public Player(Type type, int health, int power, int armor) : base(health, power, armor)
        {
            maxHealth = health;
            this.type = type;
            gold = 0;
            life = 3;
            ImageBrush ib = new ImageBrush(new BitmapImage(new Uri(string.Format("{0:d2}.bmp", type.ToString()), UriKind.Relative)));
            ib.TileMode = TileMode.Tile;
            ib.Viewport = new Rect(0, 0, 1, 1);
            ib.ViewportUnits = BrushMappingMode.Absolute;
            brush = ib;
        }

        #endregion

        #region Methods

        /// <summary>
        /// Találat éri a játékost
        /// </summary>
        /// <param name="character">a támadó karakter</param>
        /// <param name="hit">ütés nagysága</param>
        /// <returns>sebzett karakter</returns>
        public override Character Gethit(Character character, int hit)
        {
            this.Health -= (character.Power * hit) - character.Armor;
            return this;
        }

        #endregion
    }
}
