﻿namespace Escape_FF
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;
    using System.Windows.Media;
    using System.Windows.Media.Imaging;
    using GalaSoft.MvvmLight;

    /// <summary>
    /// kincs
    /// </summary>
    class Treasure : ObservableObject
    {
        #region Fields

        /// <summary>
        /// random generálás
        /// </summary>
        static Random rnd = new Random();

        /// <summary>
        /// érték
        /// </summary>
        int value;

        /// <summary>
        /// helyzet
        /// </summary>
        Point location;

        /// <summary>
        /// kép
        /// </summary>
        Brush brush;

        #endregion

        #region Properties

        /// <summary>
        /// érték t.
        /// </summary>
        public int Value
        {
            get
            {
                return value;
            }
        }

        /// <summary>
        /// kép t.
        /// </summary>
        public Brush Brush
        {
            get
            {
                return brush;
            }

            set
            {
                Set(ref brush, value);
            }
        }

        /// <summary>
        /// helyzet t.
        /// </summary>
        public Point Location
        {
            get
            {
                return location;
            }
        }

        #endregion

        #region Constructor

        /// <summary>
        /// kincs konstruktora
        /// </summary>
        /// <param name="location">helyzet</param>
        /// <param name="levelNumber">pálya szintje</param>
        public Treasure(Point location, int levelNumber)
        {
            ImageBrush ib = new ImageBrush(new BitmapImage(new Uri("gold.bmp", UriKind.Relative)));
            ib.TileMode = TileMode.Tile;
            ib.Viewport = new Rect(0, 0, 1, 1);
            ib.ViewportUnits = BrushMappingMode.Absolute;
            brush = ib;
            value = levelNumber * rnd.Next(50, 100);
            this.location = location;
        }

        #endregion
    }
}
