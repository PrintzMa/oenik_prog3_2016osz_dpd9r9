﻿namespace Escape_FF
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;
    using System.Windows.Media;
    using System.Windows.Media.Imaging;
    using GalaSoft.MvvmLight;

    /// <summary>
    /// A harc modellje
    /// </summary>
    class FightModel : ObservableObject
    {
        #region Fields

        /// <summary>
        /// játékos típusa
        /// </summary>
        string type;

        /// <summary>
        /// a játékos
        /// </summary>
        Player selectedPlayer;

        /// <summary>
        /// az ellenfél
        /// </summary>
        Enemy enemy;

        #endregion

        #region Constructor

        /// <summary>
        /// A harc modelljének konstruktora
        /// </summary>
        /// <param name="selectedPlayer">játékos</param>
        /// <param name="enemy">ellenfél</param>
        public FightModel(Player selectedPlayer, Enemy enemy)
        {
            type = selectedPlayer.Type.ToString();
            this.selectedPlayer = selectedPlayer;
            this.enemy = enemy;
        }

        #endregion

        #region Methods

        /// <summary>
        /// gomb képének forrása
        /// </summary>
        /// <param name="btnNumber">gomb</param>
        /// <returns>megjelenített gomb</returns>
        private string GetSource(int btnNumber)
        {
            string source = type + btnNumber.ToString() + ".bmp";
            return source;
        }

        #endregion

        #region Properties

        /// <summary>
        /// első gomb
        /// </summary>
        public string Btn1Source
        {
            get { return GetSource(1); }
        }

        /// <summary>
        /// második gomb
        /// </summary>
        public string Btn2Source
        {
            get { return GetSource(2); }
        }

        /// <summary>
        /// harmadik gomb
        /// </summary>
        public string Btn3Source
        {
            get { return GetSource(3); }
        }

        /// <summary>
        /// negyedik gomb
        /// </summary>
        public string Btn4Source
        {
            get { return GetSource(4); }
        }

        /// <summary>
        /// Játékos tulajdonsága
        /// </summary>
        public Player SelectedPlayer
        {
            get
            {
                return selectedPlayer;
            }

            set
            {
                selectedPlayer = value;
            }
        }

        /// <summary>
        /// ellenfél tulajdonsága
        /// </summary>
        public Enemy Enemy
        {
            get
            {
                return enemy;
            }

            set
            {
                enemy = value;
            }
        }

        #endregion
    }
}
