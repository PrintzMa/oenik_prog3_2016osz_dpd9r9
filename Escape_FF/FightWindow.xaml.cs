﻿namespace Escape_FF
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Data;
    using System.Windows.Documents;
    using System.Windows.Input;
    using System.Windows.Media;
    using System.Windows.Media.Imaging;
    using System.Windows.Shapes;

    /// <summary>
    /// Interaction logic for FightWindow.xaml
    /// </summary>
    public partial class FightWindow : Window
    {
        #region Fields

        /// <summary>
        /// randon generálás
        /// </summary>
        static Random rnd = new Random();

        /// <summary>
        /// ellenség
        /// </summary>
        Enemy enemy;

        /// <summary>
        /// A harc modellje
        /// </summary>
        FightModel FM;

        #endregion

        #region Constructor

        /// <summary>
        /// a harc ablakának konstruktora
        /// </summary>
        /// <param name="selectedPlayer">játékos</param>
        /// <param name="enemy">ellenfél</param>
        public FightWindow(Player selectedPlayer, Enemy enemy)
        {
            InitializeComponent();
            FM = new FightModel(selectedPlayer, enemy);
            this.enemy = enemy;
            this.DataContext = FM;
        }

        #endregion

        #region EventHandlers

        /// <summary>
        /// első támadás
        /// </summary>
        /// <param name="sender">s</param>
        /// <param name="e">e</param>
        private void btn1_Click(object sender, RoutedEventArgs e)
        {
            FM.SelectedPlayer.Gethit(FM.Enemy, rnd.Next(10, 20));
            FM.Enemy.Gethit(FM.SelectedPlayer, rnd.Next(20, 30));
            if (FM.Enemy.Health == 0)
            {
                MessageBox.Show("Legyőzted az ellenséget!");
                enemy.Brush = null;
                this.Close();
            }

            if (FM.SelectedPlayer.Health == 0)
            {
                FM.SelectedPlayer.Location = FM.SelectedPlayer.StartLocation;
                FM.SelectedPlayer.Life--;
                if (FM.SelectedPlayer.Life == 0)
                {
                    MessageBox.Show("Game Over!");
                    Application.Current.Shutdown();
                }
                else
                {
                    MessageBox.Show("Sajnos vesztettél, de még maradt " + FM.SelectedPlayer.Life + " életed");
                    FM.SelectedPlayer.Health = FM.SelectedPlayer.MaxHealth;
                    this.Close();
                }
            }
        }
        
        /// <summary>
        /// második támadás
        /// </summary>
        /// <param name="sender">s</param>
        /// <param name="e">e</param>
        private void btn2_Click(object sender, RoutedEventArgs e)
        {
            FM.SelectedPlayer.Gethit(FM.Enemy, rnd.Next(10, 20));
            FM.Enemy.Gethit(FM.SelectedPlayer, rnd.Next(20, 30));
            if (FM.Enemy.Health == 0)
            {
                MessageBox.Show("Legyőzted az ellenséget!");
                enemy.Brush = null;
                this.Close();
            }

            if (FM.SelectedPlayer.Health == 0)
            {
                FM.SelectedPlayer.Location = FM.SelectedPlayer.StartLocation;
                FM.SelectedPlayer.Life--;
                if (FM.SelectedPlayer.Life == 0)
                {
                    MessageBox.Show("Game Over!");
                    Application.Current.Shutdown();
                }
                else
                {
                    MessageBox.Show("Sajnos vesztettél, de még maradt " + FM.SelectedPlayer.Life + " életed");
                    FM.SelectedPlayer.Health = FM.SelectedPlayer.MaxHealth;
                    this.Close();
                }
            }
        }

        /// <summary>
        /// harmadik támadás
        /// </summary>
        /// <param name="sender">s</param>
        /// <param name="e">e</param>
        private void btn3_Click(object sender, RoutedEventArgs e)
        {
            FM.SelectedPlayer.Gethit(FM.Enemy, rnd.Next(10, 20));
            FM.Enemy.Gethit(FM.SelectedPlayer, rnd.Next(20, 30));
            if (FM.Enemy.Health == 0)
            {
                MessageBox.Show("Legyőzted az ellenséget!");
                enemy.Brush = null;
                FM.SelectedPlayer.Location = FM.SelectedPlayer.StartLocation;
                this.Close();
            }

            if (FM.SelectedPlayer.Health == 0)
            {
                FM.SelectedPlayer.Location = FM.SelectedPlayer.StartLocation;
                FM.SelectedPlayer.Life--;
                if (FM.SelectedPlayer.Life == 0)
                {
                    MessageBox.Show("Game Over!");
                    Application.Current.Shutdown();
                }
                else
                {
                    MessageBox.Show("Sajnos vesztettél, de még maradt " + FM.SelectedPlayer.Life + " életed");
                    FM.SelectedPlayer.Health = FM.SelectedPlayer.MaxHealth;
                    this.Close();
                }
            }
        }

        /// <summary>
        /// negyedik támadás
        /// </summary>
        /// <param name="sender">s</param>
        /// <param name="e">e</param>
        private void btn4_Click(object sender, RoutedEventArgs e)
        {
            FM.SelectedPlayer.Gethit(FM.Enemy, rnd.Next(10, 20));
            FM.Enemy.Gethit(FM.SelectedPlayer, rnd.Next(20, 30));
            if (FM.Enemy.Health == 0)
            {
                MessageBox.Show("Legyőzted az ellenséget!");
                enemy.Brush = null;
                this.Close();
            }

            if (FM.SelectedPlayer.Health == 0)
            {
                FM.SelectedPlayer.Location = FM.SelectedPlayer.StartLocation;
                FM.SelectedPlayer.Life--;
                if (FM.SelectedPlayer.Life == 0)
                {
                    MessageBox.Show("Game Over!");
                    Application.Current.Shutdown();
                }
                else
                {
                    MessageBox.Show("Sajnos vesztettél, de még maradt " + FM.SelectedPlayer.Life + " életed");
                    FM.SelectedPlayer.Health = FM.SelectedPlayer.MaxHealth;
                    this.Close();
                }
            }
        }

        #endregion
    }
}
