﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace Escape_FF
{
    /// <summary>
    /// Ellenfél
    /// </summary>
    public class Enemy : Character
    {
        #region Fields

        /// <summary>
        /// a játákos képe
        /// </summary>
        Brush brush;

        #endregion

        #region Properties

        /// <summary>
        /// Visszaadja/beállítja az ellenfél képét
        /// </summary>
        public Brush Brush
        {
            get
            {
                return brush;
            }

            set
            {
                Set(ref brush, value);
            }
        }

        #endregion

        #region Constructor

        /// <summary>
        /// Az ellenfél konstruktora
        /// </summary>
        /// <param name="levelNumber">pálya szintje</param>
        /// <param name="location">helyzet</param>
        public Enemy(int levelNumber, Point location) : base(50 * levelNumber, levelNumber, 10 * levelNumber)
        {
            ImageBrush ib = new ImageBrush(new BitmapImage(new Uri(string.Format("enemy{0:d2}.bmp", levelNumber), UriKind.Relative)));
            ib.TileMode = TileMode.Tile;
            ib.Viewport = new Rect(0, 0, 1, 1);
            ib.ViewportUnits = BrushMappingMode.Absolute;
            brush = ib;
            this.Location = location;
        }

        #endregion

        #region Methods

        /// <summary>
        /// támadás éri az ellenfelet
        /// </summary>
        /// <param name="character">támadó</param>
        /// <param name="hit">ütés nagysága</param>
        /// <returns>a sebzett karakter</returns>
        public override Character Gethit(Character character, int hit)
        {
            this.Health -= (character.Power * hit) - character.Armor;
            return this;
        }

        #endregion
    }
}
