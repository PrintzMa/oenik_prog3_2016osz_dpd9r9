﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Escape_FF
{
    /// <summary>
    /// Interaction logic for LevelUpWindow.xaml
    /// </summary>
    public partial class LevelUpWindow : Window
    {
        #region Fields

        /// <summary>
        /// játékos
        /// </summary>
        Player selectedPlayer;

        #endregion

        #region Constructor

        /// <summary>
        /// fejlesztés ablak konstruktora
        /// </summary>
        /// <param name="selectedPlayer">játékos</param>
        public LevelUpWindow(Player selectedPlayer)
        {
            InitializeComponent();
            this.selectedPlayer = selectedPlayer;
            this.DataContext = selectedPlayer;
        }

        #endregion

        #region EventHandlers

        /// <summary>
        /// életerő fejlesztés
        /// </summary>
        /// <param name="sender">s</param>
        /// <param name="e">e</param>
        private void btnHealth(object sender, RoutedEventArgs e)
        {
            if (selectedPlayer.Gold >= selectedPlayer.Health * 2)
            {
                selectedPlayer.Gold -= selectedPlayer.Health * 2;
                selectedPlayer.MaxHealth += 100;
                selectedPlayer.Health = selectedPlayer.MaxHealth;
            }
            else
            {
                MessageBox.Show("Nincs elég aranyad!");
            }
        }

        /// <summary>
        /// páncél fejlesztése
        /// </summary>
        /// <param name="sender">s</param>
        /// <param name="e">e</param>
        private void btnArmor(object sender, RoutedEventArgs e)
        {
            if (selectedPlayer.Gold >= selectedPlayer.Armor * 20)
            {
                selectedPlayer.Gold -= selectedPlayer.Armor * 20;
                selectedPlayer.Armor += 10;
            }
            else
            {
                MessageBox.Show("Nincs elég aranyad!");
            }
        }

        /// <summary>
        /// támadó erő ejlesztése
        /// </summary>
        /// <param name="sender">s</param>
        /// <param name="e">e</param>
        private void btnPower(object sender, RoutedEventArgs e)
        {
            if (selectedPlayer.Gold >= selectedPlayer.Power * 200)
            {
                selectedPlayer.Gold -= selectedPlayer.Power * 200;
                selectedPlayer.Power += 1;
            }
            else
            {
                MessageBox.Show("Nincs elég aranyad!");
            }
        }

        /// <summary>
        /// fejlesztés befejezése
        /// </summary>
        /// <param name="sender">s</param>
        /// <param name="e">e</param>
        private void btnDone(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        #endregion
    }
}
