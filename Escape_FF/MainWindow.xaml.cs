﻿namespace Escape_FF
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Data;
    using System.Windows.Documents;
    using System.Windows.Input;
    using System.Windows.Media;
    using System.Windows.Media.Imaging;
    using System.Windows.Navigation;
    using System.Windows.Shapes;
    using System.Windows.Threading;

    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        #region Constructor

        /// <summary>
        /// fő ablak kostruktora
        /// </summary>
        public MainWindow()
        {
            InitializeComponent();
        }

        #endregion

        #region EventHandlers

        /// <summary>
        /// játék
        /// </summary>
        /// <param name="sender">s</param>
        /// <param name="e">e</param>
        private void btnPlay(object sender, RoutedEventArgs e)
        {
            CharacterChoosing cc = new CharacterChoosing();
            this.Hide();
            cc.ShowDialog();
        }

        /// <summary>
        /// kilépés
        /// </summary>
        /// <param name="sender">s</param>
        /// <param name="e">e</param>
        private void btnExit(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        #endregion
    }
}
