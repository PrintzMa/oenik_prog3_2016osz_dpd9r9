﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;

namespace Escape_FF
{
    /// <summary>
    /// Életerőből meghatározza a szükséges aranymennyiséget a fejlesztéshez
    /// </summary>
    internal class HealthToGoldConverter : IValueConverter
    {
        /// <summary>
        /// A konverter
        /// </summary>
        /// <param name="value">value</param>
        /// <param name="targetType">targetType</param>
        /// <param name="parameter">parameter</param>
        /// <param name="culture">culture</param>
        /// <returns>aranymennyiség</returns>
        public object Convert(object value, System.Type targetType, object parameter, CultureInfo culture)
        {
            int gold = (int)value * 2;
            return gold;
        }

        /// <summary>
        /// visszakonverteráló
        /// </summary>
        /// <param name="value">value</param>
        /// <param name="targetType">targetType</param>
        /// <param name="parameter">parameter</param>
        /// <param name="culture">culture</param>
        /// <returns>exception</returns>
        public object ConvertBack(object value, System.Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    /// <summary>
    /// Páncél értékéből meghatározza a szükséges aranymennyiséget a fejlesztéshez
    /// </summary>
    internal class ArmorToGoldConverter : IValueConverter
    {
        /// <summary>
        /// a konverter
        /// </summary>
        /// <param name="value">value</param>
        /// <param name="targetType">targetType</param>
        /// <param name="parameter">parameter</param>
        /// <param name="culture">culture</param>
        /// <returns>aranymennyiség</returns>
        public object Convert(object value, System.Type targetType, object parameter, CultureInfo culture)
        {
            int gold = (int)value * 20;
            return gold;
        }

        /// <summary>
        /// visszakonvertá
        /// </summary>
        /// <param name="value">value</param>
        /// <param name="targetType">targetType</param>
        /// <param name="parameter">parameter</param>
        /// <param name="culture">culture</param>
        /// <returns>exception</returns>
        public object ConvertBack(object value, System.Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    /// <summary>
    /// A támadó erőből meghatározza a szükséges aranymennyiséget a fejlesztéshez
    /// </summary>
    internal class PowerToGoldConverter : IValueConverter
    {
        /// <summary>
        /// a konverter
        /// </summary>
        /// <param name="value">value</param>
        /// <param name="targetType">targeType</param>
        /// <param name="parameter">parameter</param>
        /// <param name="culture">culture</param>
        /// <returns>aranymennyiség</returns>
        public object Convert(object value, System.Type targetType, object parameter, CultureInfo culture)
        {
            int gold = (int)value * 200;
            return gold;
        }

        /// <summary>
        /// visszakonvertáló
        /// </summary>
        /// <param name="value">value</param>
        /// <param name="targetType">t</param>
        /// <param name="parameter">p</param>
        /// <param name="culture">c</param>
        /// <returns>exception</returns>
        public object ConvertBack(object value, System.Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    /// <summary>
    /// Meghatározza a játékos képének helyét
    /// </summary>
    internal class PlayerToImageSourceConverter : IValueConverter
    {
        /// <summary>
        /// konverter
        /// </summary>
        /// <param name="value">v</param>
        /// <param name="targetType">t</param>
        /// <param name="parameter">p</param>
        /// <param name="culture">c</param>
        /// <returns>forrás</returns>
        public object Convert(object value, System.Type targetType, object parameter, CultureInfo culture)
        {
            Player player = (Player)value;
            string source = player.Type.ToString() + ".bmp";
            return source;
        }

        /// <summary>
        /// visszakonvertáló
        /// </summary>
        /// <param name="value">v</param>
        /// <param name="targetType">t</param>
        /// <param name="parameter">p</param>
        /// <param name="culture">c</param>
        /// <returns>exception</returns>
        public object ConvertBack(object value, System.Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
