﻿namespace Escape_FF
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;
    using GalaSoft.MvvmLight;

    /// <summary>
    /// Absztrakt karakter osztály, amelyből öröklődni fog a Játékos, és az Ellenfél
    /// </summary>
    public abstract class Character : ObservableObject
    {
        #region Fields

        /// <summary>
        /// Életerő
        /// </summary>
        int health;

        /// <summary>
        /// Támadó erő
        /// </summary>
        int power;

        /// <summary>
        /// Páncélzat
        /// </summary>
        int armor;

        /// <summary>
        /// Helyzet a térképen
        /// </summary>
        Point location;

        #endregion

        #region Properties

        /// <summary>
        /// Visszaadja/beállítja az életerőt
        /// </summary>
        public int Health
        {
            get
            {
                return health;
            }

            set
            {
                if (value > 0)
                {
                    Set(ref health, value);
                }
                else
                {
                    Set(ref health, 0);
                }
            }
        }

        /// <summary>
        /// Visszaadja/beállítja a támadó erőt
        /// </summary>
        public int Power
        {
            get
            {
                return power;
            }

            set
            {
                Set(ref power, value);
            }
        }

        /// <summary>
        /// Visszaadja/beállítja a páncél értékét
        /// </summary>
        public int Armor
        {
            get
            {
                return armor;
            }

            set
            {
                Set(ref armor, value);
            }
        }

        /// <summary>
        /// Visszaadja/beállítja a helyzetet
        /// </summary>
        public Point Location
        {
            get
            {
                return location;
            }

            set
            {
                Set(ref location, value);
            }
        }

        #endregion

        #region Constructor

        /// <summary>
        /// karakter konstruktora
        /// </summary>
        /// <param name="health">életerő</param>
        /// <param name="power">támadó erő</param>
        /// <param name="armor">páncél</param>
        public Character(int health, int power, int armor)
        {
            this.health = health;
            this.power = power;
            this.armor = armor;
        }

        #endregion

        #region Methods

        /// <summary>
        /// Találat éri a karaktert
        /// </summary>
        /// <param name="character">karakter</param>
        /// <param name="hit">ütéshez nagyságát meghatározza</param>
        /// <returns>visszaadja a sebzett karaktert</returns>
        public abstract Character Gethit(Character character, int hit);

        #endregion
    }
}
