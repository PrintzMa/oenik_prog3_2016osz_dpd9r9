﻿namespace Escape_FF
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;
    using System.Windows.Data;
    using System.Windows.Media;

    /// <summary>
    /// pozició konverter - kép megjelenítése
    /// </summary>
    class PositionToGeometryConverter : IValueConverter
    {
        /// <summary>
        /// konverter
        /// </summary>
        /// <param name="value">v</param>
        /// <param name="targetType">t</param>
        /// <param name="parameter">p</param>
        /// <param name="culture">c</param>
        /// <returns>kép</returns>
        public object Convert(object value, System.Type targetType, object parameter, CultureInfo culture)
        {
            Point position = (Point)value;
            return new RectangleGeometry(new Rect(position.X, position.Y, 1, 1));
        }

        /// <summary>
        /// visszakonveetáló
        /// </summary>
        /// <param name="value">v</param>
        /// <param name="targetType">t</param>
        /// <param name="parameter">p</param>
        /// <param name="culture">c</param>
        /// <returns>exception</returns>
        public object ConvertBack(object value, System.Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
