﻿namespace Escape_FF
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;
    using System.Windows.Media;

    /// <summary>
    /// Az üzleti logika elkülönítése
    /// </summary>

    internal class BusinessLogic
    {
        #region Fields

        /// <summary>
        /// Az ellenségek száma
        /// </summary>
        private int numEnemy;

        /// <summary>
        /// A kincsek száma
        /// </summary>
        private int numTreasure;
        #endregion

        #region Constructor

        /// <summary>
        /// kiolvassa és létrehozza a pályát.
        /// </summary>
        /// <param name="levelNumber"> A pálya szintje</param>
        /// <param name="selectedPlayer"> A játékos</param>
        public BusinessLogic(int levelNumber, Player selectedPlayer)
        {
            this.MM = new MapModel(selectedPlayer);
            string fileName = string.Format("L{0:d2}.lvl", levelNumber);
            string[] lines = File.ReadAllLines(fileName);
            int xSize = int.Parse(lines[0]);
            int ySize = int.Parse(lines[1]);
            this.MM.Map = new bool[xSize, ySize];
            this.MM.Enemies = new List<Enemy>();
            this.MM.Treasures = new List<Treasure>();
            for (int y = 2; y < lines.Length; y++)
            {
                for (int x = 0; x < lines[y].Length; x++)
                {
                    this.MM.Map[x, y - 2] = lines[y][x] == 'x';
                    if (lines[y][x] == 'P')
                    {
                        this.MM.SelectedPlayer.StartLocation = new Point(x, y - 2);
                        this.MM.SelectedPlayer.Location = new Point(x, y - 2);
                    }

                    if (lines[y][x] == 'O')
                    {
                        this.MM.Exit = new Point(x, y - 2);
                    }

                    if (lines[y][x] == 'E')
                    {
                        this.MM.Enemies.Add(new Enemy(levelNumber, new Point(x, y - 2)));
                    }

                    if (lines[y][x] == 'G')
                    {
                        this.MM.Treasures.Add(new Treasure(new Point(x, y - 2), levelNumber));
                    }
                }
            }

            numEnemy = this.MM.Enemies.Count;
            numTreasure = this.MM.Treasures.Count;
        }
        #endregion

        #region Methods

        /// <summary>
        /// A mozgás metódusa
        /// </summary>
        /// <param name="dx">x koordináta változása</param>
        /// <param name="dy">y koordináta változása</param>
        /// <returns>Visszaadja, hogy a játékos elérte e már a kijáratot</returns>
        public bool Move(int dx, int dy)
        {
            int newx = (int)(this.MM.SelectedPlayer.Location.X + dx);
            int newy = (int)(this.MM.SelectedPlayer.Location.Y + dy);
            Point newPoint = new Point(newx, newy);
            if (newx >= 0 && newx < MM.Map.GetLength(0) &&
               newy >= 0 && newy < MM.Map.GetLength(1) &&
               !MM.Map[newx, newy])
            {
                for (int i = 0; i < numEnemy; i++)
                {
                    if (MM.Enemies[i].Location == newPoint && MM.Enemies[i].Health > 0)
                    {
                        FightWindow fw = new FightWindow(MM.SelectedPlayer, MM.Enemies[i]);
                        fw.ShowDialog();
                    }
                }

                for (int j = 0; j < numTreasure; j++)
                {
                    if (MM.Treasures[j].Location == newPoint)
                    {
                        MM.Treasures[j].Brush = null;
                        MM.SelectedPlayer.Gold += MM.Treasures[j].Value;
                        MM.Treasures.Remove(MM.Treasures[j]);
                        numTreasure--;
                    }
                }

                MM.SelectedPlayer.Location = newPoint;
            }

            return MM.SelectedPlayer.Location.Equals(MM.Exit);
        }
        
        #endregion

        #region Properties

        /// <summary>
        /// Az üzleti logikához tartozó MapModel
        /// </summary>
        public MapModel MM { get; private set; }

        #endregion
    }
}
