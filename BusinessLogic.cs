﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Escape_FF
{
    class BusinessLogic
    {
        
    }
    class PlayerBL
    {
        Character player;
        public enum Direction { Left, Right, Up, Down };
        public void Move(Direction direction)
        {
            switch (direction)
            {
                case Direction.Left:
                    player.IncreaseX(-10);
                    break;
                case Direction.Right:
                    player.IncreaseX(10);
                    break;
                case Direction.Up:
                    player.IncreaseY(-10);
                    break;
                case Direction.Down:
                    player.IncreaseY(10);
                    break;
                default:
                    break;
            }

        }
        public PlayerBL(Character player)
        {
            this.player = player;
        }

    }
    class EnemyBL
    {
        Character enemy;
        int dx = 3;
        int dy = 3;
        public EnemyBL(Character enemy)
        {
            this.enemy = enemy;
        }
        public bool Move(int canvasWidth, int canvasHeight, Character player)
        {
            enemy.IncreaseX(dx);
            if (enemy.Area.Left < 0 || enemy.Area.Right > canvasWidth)
            {
                dx = -dx;
            }
            if (enemy.Area.Top < 0 || enemy.Area.IntersectsWith(player.Area))
            {
                dx = -dx;
                return true;
            }
            else return false;
        }
    }
}
