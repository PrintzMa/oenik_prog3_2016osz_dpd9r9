﻿using GalaSoft.MvvmLight;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Escape_FF
{
    class ViewModel : ViewModelBase
    {
        const int canvasWidth =200;
        const int canvasHeight =200;
        public static ViewModel peldany;
        public static ViewModel Get()
        {
            if (peldany == null) peldany = new ViewModel();
            return peldany;
        }
        int health;
        public Character Player { get; private set; }
        public Character Enemy { get; private set; }

        public int Health
        {
            get
            {
                return health;
            }

            set
            {
                Set(ref health, value);
            }
        }

        public ViewModel()
        {
            Player = new Character(canvasWidth / 2, canvasHeight - 20, 30, 30);
            Enemy = new Character(canvasWidth / 2, canvasHeight / 2, 30, 30);
            health = 100;
        }
    }
}
