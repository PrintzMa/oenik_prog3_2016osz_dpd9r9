﻿using GalaSoft.MvvmLight;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Escape_FF
{
    class Character : ObservableObject
    {
        Rect area;

        public Rect Area
        {
            get
            {
                return area;
            }
            
        }
        public Character(int x, int y, int width, int height)
        {
            this.area = new Rect(x, y, width, height);
        }
        public void IncreaseX(int diff)
        {
            area.X += diff;
            this.RaisePropertyChanged("Area");
        }
        public void IncreaseY(int diff)
        {
            area.Y += diff;
            this.RaisePropertyChanged("Area");
        }
        public void SetXY(int x, int y)
        {
            area.X = x;
            area.Y = y;
            this.RaisePropertyChanged("Area");
        }
    }
}
