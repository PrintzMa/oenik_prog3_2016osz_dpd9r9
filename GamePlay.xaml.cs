﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace Escape_FF
{
    /// <summary>
    /// Interaction logic for GamePlay.xaml
    /// </summary>
    public partial class GamePlay : Window
    {
        ViewModel vm;
        public GamePlay()

        {
            vm = ViewModel.Get();
            this.DataContext = vm;
            InitializeComponent();
        }
        PlayerBL PL;
        EnemyBL EL;
        int cw, ch;
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            Canvas c = this.Content as Canvas;
            cw = (int)c.ActualWidth;
            ch = (int)c.ActualHeight;
            vm = new ViewModel();
            this.DataContext = vm;
            PL = new PlayerBL(vm.Player);
            EL = new EnemyBL(vm.Enemy);
            DispatcherTimer timer = new DispatcherTimer();
            timer.Interval = TimeSpan.FromMilliseconds(100);
            timer.Tick += Timer_Tick;
            timer.Start();
        }

        private void Timer_Tick(object sender, EventArgs e)
        {
            if (EL.Move(cw, ch, vm.Player) == true)
            {
                vm.Health -= 10;
            }
        }

        private void Window_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.Key)
            {
                case Key.Left:
                    PL.Move(PlayerBL.Direction.Left);
                    break;
                case Key.Right:
                    PL.Move(PlayerBL.Direction.Right);
                    break;
                case Key.Up:
                    PL.Move(PlayerBL.Direction.Up);
                    break;
                case Key.Down:
                    PL.Move(PlayerBL.Direction.Down);
                    break;
            }
        }
    }
}
